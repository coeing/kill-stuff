﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Weapons.Components
{
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;

    [InspectorComponent]
    public class WeaponComponent : EntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Number of bullets left.
        /// </summary>
        public const string AttributeBulletCount = "WeaponComponent.BulletCount";

        /// <summary>
        ///   Attribute: Maximum number of bullets.
        /// </summary>
        public const string AttributeMaxBulletCount = "WeaponComponent.MaxBulletCount";

        /// <summary>
        ///   Attribute: Projectiles this weapon fires.
        /// </summary>
        public const string AttributeProjectileBlueprintId = "WeaponComponent.ProjectileBlueprintId";

        /// <summary>
        ///   Attribute: Speed with which the bullets are fired.
        /// </summary>
        public const string AttributeSpeed = "WeaponComponent.Speed";

        /// <summary>
        ///   Attribute default: Number of bullets left.
        /// </summary>
        public const int DefaultBulletCount = 0;

        /// <summary>
        ///   Attribute default: Maximum number of bullets.
        /// </summary>
        public const int DefaultMaxBulletCount = 10;

        /// <summary>
        ///   Attribute default: Projectiles this weapon fires.
        /// </summary>
        public const string DefaultProjectileBlueprintId = null;

        /// <summary>
        ///   Attribute default: Speed with which the bullets are fired.
        /// </summary>
        public const float DefaultSpeed = 0;

        #endregion

        #region Properties

        /// <summary>
        ///   Number of bullets left.
        /// </summary>
        [InspectorInt(AttributeBulletCount, Default = DefaultBulletCount, Description = "Number of bullets left.")]
        public int BulletCount { get; set; }

        /// <summary>
        ///   Maximum number of bullets.
        /// </summary>
        [InspectorInt(AttributeMaxBulletCount, Default = DefaultMaxBulletCount,
            Description = "Maximum number of bullets.")]
        public int MaxBulletCount { get; set; }

        /// <summary>
        ///   Projectiles this weapon fires.
        /// </summary>
        [InspectorString(AttributeProjectileBlueprintId, Default = DefaultProjectileBlueprintId,
            Description = "Projectiles this weapon fires.")]
        public string ProjectileBlueprintId { get; set; }

        /// <summary>
        ///   Speed with which the bullets are fired.
        /// </summary>
        [InspectorFloat(AttributeSpeed, Default = DefaultSpeed, Description = "Speed with which the bullets are fired.")
        ]
        public float Speed { get; set; }

        #endregion
    }
}