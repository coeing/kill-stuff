﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AnchorComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Weapons.Components
{
    using Slash.ECS.Components;
    using Slash.ECS.Inspector.Attributes;
    using Slash.Math.Algebra.Vectors;

    [InspectorType]
    public class AnchorComponent : EntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Relative anchor offset.
        /// </summary>
        public const string AttributeOffset = "AnchorComponent.Offset";

        #endregion

        #region Properties

        /// <summary>
        ///   Relative anchor offset.
        /// </summary>
        [InspectorVector(AttributeOffset, Description = "Relative anchor offset.")]
        public Vector2F Offset { get; set; }

        #endregion
    }
}