﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Weapons.Systems
{
    using KillStuff.Logic.Features.Movement.Components;
    using KillStuff.Logic.Features.Weapons.Components;
    using KillStuff.Logic.Features.Weapons.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;
    using Slash.Math.Algebra.Vectors;

    [GameSystem]
    public class WeaponSystem : GameSystem
    {
        #region Fields

        private CompoundEntities<WeaponEntity> weaponEntities;

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(WeaponAction.Fire, this.OnFire);
            this.EventManager.RegisterListener(WeaponAction.Reload, this.OnReload);

            this.weaponEntities = new CompoundEntities<WeaponEntity>(this.EntityManager);
        }

        #endregion

        #region Methods

        private void OnFire(GameEvent e)
        {
            // Get entity which fired.
            var entityId = (int)e.EventData;
            var weaponEntity = this.weaponEntities.GetEntity(entityId);
            if (weaponEntity == null)
            {
                return;
            }

            // Check if bullets left.
            if (weaponEntity.Weapon.BulletCount <= 0)
            {
                // No bullets left.
                return;
            }

            // Shoot.
            var projectileEntityId = this.Shoot(entityId, weaponEntity);

            // Decrease bullets.
            --weaponEntity.Weapon.BulletCount;

            // Fire event.
            this.EventManager.QueueEvent(
                WeaponEvent.Fired,
                new WeaponFiredData { WeaponEntityId = entityId, BulletEntityId = projectileEntityId });
        }

        private void OnReload(GameEvent e)
        {
            // Get entity which should be reloaded.
            var entityId = (int)e.EventData;
            var weaponEntity = this.weaponEntities.GetEntity(entityId);
            if (weaponEntity == null)
            {
                return;
            }

            // Reload weapon if it isn't full of bullets already.
            if (weaponEntity.Weapon.BulletCount >= weaponEntity.Weapon.MaxBulletCount)
            {
                return;
            }

            weaponEntity.Weapon.BulletCount = weaponEntity.Weapon.MaxBulletCount;
            this.EventManager.QueueEvent(WeaponEvent.Reloaded, entityId);
        }

        private int Shoot(int entityId, WeaponEntity weaponEntity)
        {
            var transformComponent = this.EntityManager.GetComponent<TransformComponent>(entityId);

            // Compute initial position.
            var position = transformComponent.Position;
            if (weaponEntity.Anchor != null)
            {
                position += weaponEntity.Anchor.Offset;
            }

            // Compute initial velocity.
            var velocity = Vector2F.UnitX * weaponEntity.Weapon.Speed;

            // Gather initial attributes of projectile.
            var configuration = new AttributeTable
            {
                { TransformComponent.AttributePosition, position },
                { MovementComponent.AttributeVelocity, velocity }
            };

            // Create projectile.
            var projectileBlueprint = this.BlueprintManager.GetBlueprint(weaponEntity.Weapon.ProjectileBlueprintId);
            var projectileEntityId = this.EntityManager.CreateEntity(projectileBlueprint, configuration);

            return projectileEntityId;
        }

        #endregion

        private class WeaponEntity
        {
            #region Fields

            [CompoundComponent(IsOptional = true)]
            public AnchorComponent Anchor;

            [CompoundComponent]
            public WeaponComponent Weapon;

            #endregion
        }
    }
}