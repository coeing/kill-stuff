﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponFiredData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Weapons.Events
{
    public class WeaponFiredData
    {
        #region Properties

        public int BulletEntityId { get; set; }

        public int WeaponEntityId { get; set; }

        #endregion
    }
}