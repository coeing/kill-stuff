﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponAction.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Weapons.Events
{
    using Slash.ECS.Events;

    [GameEventType]
    public enum WeaponAction
    {
        Fire,

        Reload,
    }
}