﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Input.Systems
{
    using KillStuff.Logic.Features.Input.Components;
    using KillStuff.Logic.Features.Input.Events;
    using KillStuff.Logic.Features.Movement.Components;
    using KillStuff.Logic.Features.Weapons.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Systems;
    using Slash.Math.Algebra.Vectors;
    using Slash.SystemExt.Utils;

    [GameSystem]
    public class InputSystem : GameSystem
    {
        #region Fields

        private CompoundEntities<InputEntity> inputEntities;

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(InputAction.Move, this.OnMove);
            this.EventManager.RegisterListener(InputAction.Fire, this.OnFire);
            this.EventManager.RegisterListener(InputAction.Reload, this.OnReload);

            this.inputEntities = new CompoundEntities<InputEntity>(this.EntityManager);
        }

        #endregion

        #region Methods

        private static Vector2I GetDirection(MoveDirection moveDirections)
        {
            var direction = new Vector2I();
            if (moveDirections.IsOptionSet(MoveDirection.Forward))
            {
                direction.Y += 1;
            }
            if (moveDirections.IsOptionSet(MoveDirection.Backward))
            {
                direction.Y -= 1;
            }
            if (moveDirections.IsOptionSet(MoveDirection.Right))
            {
                direction.X += 1;
            }
            if (moveDirections.IsOptionSet(MoveDirection.Left))
            {
                direction.X -= 1;
            }
            return direction;
        }

        private void OnFire(GameEvent e)
        {
            var entityId = (int)e.EventData;

            var inputEntity = this.inputEntities.GetEntity(entityId);
            if (inputEntity == null)
            {
                return;
            }

            // Forward action to weapon feature.
            this.EventManager.QueueEvent(WeaponAction.Fire, entityId);
        }

        private void OnMove(GameEvent e)
        {
            MoveData data = (MoveData)e.EventData;

            InputEntity inputEntity = this.inputEntities.GetEntity(data.EntityId);
            if (inputEntity == null)
            {
                return;
            }

            // Adjust move direction.
            if (data.Enable)
            {
                inputEntity.Input.MoveDirections =
                    (MoveDirection)inputEntity.Input.MoveDirections.OrOption(data.Direction, typeof(MoveDirection));
            }
            else
            {
                inputEntity.Input.MoveDirections =
                    (MoveDirection)
                        inputEntity.Input.MoveDirections.AndComplementOption(data.Direction, typeof(MoveDirection));
            }

            // Adjust velocity.
            inputEntity.Movement.Velocity = GetDirection(inputEntity.Input.MoveDirections)
                                            * inputEntity.Movement.MaxSpeed;
        }

        private void OnReload(GameEvent e)
        {
            var entityId = (int)e.EventData;

            var inputEntity = this.inputEntities.GetEntity(entityId);
            if (inputEntity == null)
            {
                return;
            }

            // Forward action to weapon feature.
            this.EventManager.QueueEvent(WeaponAction.Reload, entityId);
        }

        #endregion

        private class InputEntity
        {
            #region Fields

            [CompoundComponent]
            public InputComponent Input;

            [CompoundComponent]
            public MovementComponent Movement;

            #endregion
        }
    }
}