﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputAction.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Input.Events
{
    public enum InputAction
    {
        /// <summary>
        ///   Indicates to start/stop moving in a direction.
        ///   <para>
        ///     Event data: <see cref="MoveData"/>.
        ///   </para>
        /// </summary>
        Move,

        /// <summary>
        ///   Input to fire the weapon.
        /// </summary>
        Fire,

        /// <summary>
        ///   Input to reload the weapon.
        /// </summary>
        Reload,
    }
}