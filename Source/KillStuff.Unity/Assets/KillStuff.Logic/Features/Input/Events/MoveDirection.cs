﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MoveDirection.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Input.Events
{
    using System;

    [Flags]
    public enum MoveDirection
    {
        None,

        Forward = 1 << 1,

        Backward = 1 << 2,

        Left = 1 << 3,

        Right = 1 << 4,
    }
}