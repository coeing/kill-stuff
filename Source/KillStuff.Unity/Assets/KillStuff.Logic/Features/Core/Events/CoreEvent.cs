﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CoreEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Core.Events
{
    using Slash.ECS.Events;

    [GameEventType]
    public enum CoreEvent
    {
        /// <summary>
        ///   The player character was created.
        ///   <para>
        ///     Data: Int (Entity id of player character).
        ///   </para>
        /// </summary>
        PlayerCreated
    }
}