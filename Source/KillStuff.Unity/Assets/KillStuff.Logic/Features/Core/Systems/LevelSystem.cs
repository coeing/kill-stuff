﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LevelSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Core.Systems
{
    using KillStuff.Logic.Features.Core.Events;

    using Slash.Collections.AttributeTables;
    using Slash.ECS.Events;
    using Slash.ECS.Inspector.Attributes;
    using Slash.ECS.Systems;

    [GameSystem]
    [InspectorType]
    public class LevelSystem : GameSystem
    {
        #region Constants

        /// <summary>
        ///   Attribute: Id of player blueprint.
        /// </summary>
        public const string AttributePlayerBlueprintId = "LevelSystem.PlayerBlueprintId";

        /// <summary>
        ///   Attribute default: Id of player blueprint.
        /// </summary>
        public const string DefaultPlayerBlueprintId = "Player";

        #endregion

        #region Properties

        /// <summary>
        ///   Id of player blueprint.
        /// </summary>
        [InspectorBlueprint(AttributePlayerBlueprintId, Default = DefaultPlayerBlueprintId,
            Description = "Id of player blueprint.")]
        public string PlayerBlueprintId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.EventManager.RegisterListener(FrameworkEvent.GameStarted, this.OnGameStarted);
        }

        #endregion

        #region Methods

        private void CreatePlayerCharacter()
        {
            // Create player entity.
            var playerConfiguration = new AttributeTable();
            var playerEntityId = this.EntityManager.CreateEntity(this.PlayerBlueprintId, playerConfiguration);
            
            this.EventManager.QueueEvent(CoreEvent.PlayerCreated, playerEntityId);
        }

        private void OnGameStarted(GameEvent e)
        {
            this.CreatePlayerCharacter();
        }

        #endregion
    }
}