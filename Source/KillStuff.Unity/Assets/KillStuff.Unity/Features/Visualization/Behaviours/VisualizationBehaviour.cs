﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisualizationBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Visualization.Behaviours
{
    using KillStuff.Logic.Features.Core.Components;

    using Slash.Unity.Common.ECS;
    using Slash.Unity.Common.Utils;

    using UnityEngine;

    public class VisualizationBehaviour : EntityComponentBehaviour<VisualizationComponent>
    {
        #region Methods

        protected override void Start()
        {
            base.Start();

            this.CreateVisualization();
        }

        private void CreateVisualization()
        {
            if (this.Component == null || string.IsNullOrEmpty(this.Component.Prefab))
            {
                return;
            }

            // Create prefab.
            var visualization = this.gameObject.AddChild(Resources.Load<GameObject>(this.Component.Prefab));
            visualization.name = "Visualization";
        }

        #endregion
    }
}