﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DummyWeaponContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Weapons.Contexts
{
    public class DummyWeaponContext : WeaponContext
    {
        #region Constructors and Destructors

        public DummyWeaponContext()
        {
            this.CurrentBulletCount = 3;
            this.MaxBulletCount = 10;
        }

        #endregion
    }
}