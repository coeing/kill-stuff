﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponContextInitializer.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Weapons.Contexts
{
    using KillStuff.Unity.Features.Core.Contexts;

    using Slash.Unity.DataBind.Core.Presentation;

    using UnityEngine;

    public class WeaponContextInitializer : MonoBehaviour
    {
        #region Fields

        public ContextHolder ContextHolder;

        public GameRootContext RootContext;

        #endregion

        #region Methods

        protected void Start()
        {
            if (this.RootContext == null)
            {
                this.RootContext = FindObjectOfType<GameRootContext>();
            }
            if (this.ContextHolder == null)
            {
                this.ContextHolder = this.gameObject.GetComponent<ContextHolder>();
            }

            if (this.RootContext != null)
            {
                this.ContextHolder.Context = this.RootContext.Weapon;
            }
        }

        #endregion
    }
}